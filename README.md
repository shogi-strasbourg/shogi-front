# Shogi-front

Le front-end du site de l'Association de Shogi d'Alsace - Section Strasbourg

## Technologies utilisées

Vue3, Vite, TypeScript

## Scripts:

"dev": "vite",

"build": "vue-tsc && vite build",

"preview": "vite preview"