export interface JWT {
    "token_type": string,
    "exp": number,
    "iat": number,
    "jti": string,
    "user_id": number
}

export interface User {
    "id": number,
    "name": string,
    "email": string,
    "username": string,
    "is_staff": boolean,
}

export interface Staff {
    id: number,
    is_staff: boolean,
}