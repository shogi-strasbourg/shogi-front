export interface ArticlesApi {
    author: { username: string, first_name: string },
    brief: string,
    category: { id: number, name: string, description: string }[],
    content: string,
    created_at: string,
    highlight: boolean,
    highlight_end: Date | null,
    id: number,
    image: {
        image: string,
        alt: string,
    },
    slug: string,
    title: string,
    updated_at: string
}

export interface Categories {
    description: string,
    id: number,
    name: string,
}