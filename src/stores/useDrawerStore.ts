import {defineStore} from "pinia";
import {ref} from "vue";

export const useDrawerStore = defineStore('drawer', () => {
    const drawerDisplay = ref(false)

    const toggleDrawer = () => drawerDisplay.value = !drawerDisplay.value

    return {toggleDrawer, drawerDisplay}
})