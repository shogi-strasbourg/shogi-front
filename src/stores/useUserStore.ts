import {defineStore} from "pinia";
import {ref} from "vue";
import type {JWT, User} from "#/JWT.d.ts";
import {useLogin} from "@/composables/useLogin.ts";

export const useUserStore = defineStore('user', () => {

    const {getUserMe} = useLogin()

    const jwt = ref<JWT | undefined>()

    const isLogin = ref<boolean>(true)

    const userData = ref<User | undefined>()

    const jwtIsValid = () => !!jwt.value

    const jwtHasExpired = () => {
        if (!jwt.value) return true
        return jwt.value.exp < Math.floor(Date.now() / 1000 + 5)
    }

    const setJwt = (newJwt: JWT | undefined) => {
        jwt.value = newJwt
    }

    const userIsLogged = () => jwtIsValid() && !jwtHasExpired()

    const setUserData = (newUserData: any) => {
        userData.value = newUserData
    }

    const setJwtWrapper = async () => {
        if (localStorage.getItem('jwt_access')) {
            setJwt({...JSON.parse(window.atob((localStorage.getItem('jwt_access') as string).split('.')[1]))})
            await getUserMe()
        }
    }
    
    return {
        isLogin,
        jwt,
        jwtHasExpired,
        jwtIsValid,
        setJwt,
        setJwtWrapper,
        setUserData,
        userData,
        userIsLogged,
    }
})