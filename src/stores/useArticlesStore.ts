import type {ArticlesApi} from "#/Articles";
import {ref} from "vue";
import {defineStore} from "pinia";

export const useArticlesStore = defineStore('articles', () => {
    const articles = ref<ArticlesApi[]>()
    const articleDetailed = ref<ArticlesApi>()
    const highlightedArticle = ref<ArticlesApi>()

    function setArticles(articlesArray: ArticlesApi[]) {
        articles.value = articlesArray
    }

    function setArticleDetailed(article: ArticlesApi) {
        articleDetailed.value = article
    }

    function setHighlightedArticle(highlighted: ArticlesApi) {
        highlightedArticle.value = highlighted
    }

    return {
        articles,
        articleDetailed,
        highlightedArticle,
        setArticles,
        setArticleDetailed,
        setHighlightedArticle,
    }
})