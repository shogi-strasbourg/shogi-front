import {RouteRecordRaw, createRouter, createWebHistory} from 'vue-router'
import {useUserStore} from "@/stores/useUserStore.ts";
import axios from "axios";
import {useCustomQuasar} from "@/composables/useCustomQuasar.ts";


const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        component: () => import('@/views/HomePageView.vue'),
        name: 'home'
    },
    {
        path: '/article/:slug',
        component: () => import('@/views/ArticleView.vue'),
        name: 'article'
    },
    {
        path: '/presentation',
        component: () => import('@/views/PresentationView.vue'),
        name: 'presentation'
    },
    {
        path: '/login',
        component: () => import('@/views/LoginRegisterView.vue'),
        name: 'login',
        meta: {
            isForbiddenAuth: true
        }
    },
    {
        path: '/nous-contacter',
        component: () => import('@/views/ContactView.vue'),
        name: 'contact'
    },
    {
        path: '/qui-sommes-nous',
        component: () => import('@/views/UsView.vue'),
        name: 'us'
    },
    {
        path: '/tournois',
        component: () => import('@/views/TournamentsView.vue'),
        name: 'tournaments'
    },
    {
        path: '/calendrier',
        component: () => import('@/views/CalendarView.vue'),
        name: 'calendar'
    },
    {
        path: '/admin',
        component: () => import('@/views/AdminView/AdminTemplate.vue'),
        name: 'admin-template',
        children: [
            {
                path: '',
                component: () => import('@/views/AdminView/AdminView.vue'),
                name: 'admin'
            },
        ],
        meta: {
            requiresAdmin: true
        }
    },
    {
        path: '/newsletter/verify/:token',
        component: () => import('@/views/NewsletterVerifyView.vue'),
        name: 'newsletter-verify'
    },
    {
        path: '/newsletter/unregister/',
        component: () => import('@/views/NewsletterUnregisterView.vue'),
        name: 'newsletter-unregister'
    },
    {
        path: '/mentions-legales',
        component: () => import('@/views/LegalInformations.vue'),
        name: 'legal-informations'
    },
    {
        path: '/politique-confidentialite',
        component: () => import('@/views/PrivacyPolicy.vue'),
        name: 'privacy-policy'
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior: (to) => {
        if (to.hash) {
            return {
                el: to.hash,
                behavior: 'smooth',
            }
        }
        return {
            left: 0,
            top: 0
        }
    },
})

router.beforeEach(async (to, _from, next) => {
    const isRequiredAuth = to.matched.some(record => record.meta.requiresAuth)
    const isRequiredAdmin = to.matched.some(record => record.meta.requiresAdmin)
    const isForbiddenAuth = to.matched.some(record => record.meta.isForbiddenAuth)
    const userStore = useUserStore()
    const {
        jwtIsValid,
        jwtHasExpired,
        userIsLogged,
        setJwt,
    } = userStore

    const {customNotify} = useCustomQuasar()

    if (isForbiddenAuth) {
        // L'utilisateur ne doit pas être connecté
        if (userIsLogged()) {
            customNotify('Vous êtes déjà connecté', 'negative')
            next({name: 'home'})
        } else {
            next()
        }
    } else if (!isRequiredAdmin && !isRequiredAuth) {
        // L'utilisateur n'a pas besoin d'être connecté
        next()
    } else if (isRequiredAdmin || isRequiredAuth) {
        // L'utilisateur doit être connecté
        if (!userIsLogged()) {
            // L'utilisateur n'est pas connecté
            if (jwtIsValid() && jwtHasExpired()) {
                // Le token est valide mais a expiré
                // On le rafraichit
                const apiBaseUrl = import.meta.env.VITE_APP_BACKEND_BASEURL
                const response = await axios.post(apiBaseUrl + 'auth/token/refresh/', {
                    refresh: localStorage.getItem('jwt_refresh')
                })
                if (response.status === 200) {
                    localStorage.setItem('jwt_access', response.data.access)
                    setJwt(response.data.access)
                    customNotify('Jwt refesh', 'positive')
                } else {
                    // Le token refresh n'est plus valide
                    // On redirige vers la page de connexion
                    customNotify('refresh invalide', 'negative')
                    customNotify('Vous devez être connecté pour accéder à cette page', 'negative')
                    return next({name: 'login'})
                }
            } else {
                // Le token n'est pas valide ou n'existe pas
                // On redirige vers la page de connexion
                customNotify('Vous devez être connecté pour accéder à cette page', 'negative')
                return next({name: 'login'})
            }
        }
        // L'utilisateur est connecté
        if (!isRequiredAdmin) {
            // L'utilisateur n'a pas besoin d'être admin
            next()
        } else {
            // L'utilisateur doit être admin
            const apiBaseUrl = import.meta.env.VITE_APP_BACKEND_BASEURL
            const response = await axios.get(apiBaseUrl + 'auth/permission/', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('jwt_access')
                }
            })
            if (response.status === 200 && response.data.is_staff === true) {
                // L'utilisateur est admin
                next()
            } else {
                // L'utilisateur n'est pas admin
                customNotify('Vous n\'avez pas les permissions nécessaires pour accéder à cette page', 'negative')
                next({name: 'home'})
            }
        }
    }
})

export default router