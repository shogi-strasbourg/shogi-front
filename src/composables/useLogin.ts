import {ref} from "vue";
import axios from "axios";
import {storeToRefs} from "pinia";

import {useUserStore} from "@/stores/useUserStore.ts";
import {useCustomQuasar} from "@/composables/useCustomQuasar.ts";
import {useRouter, useRoute} from "vue-router";


export const useLogin = () => {

    const router = useRouter()
    const route = useRoute()
    const userStore = useUserStore()

    const {
        setJwt,
        userIsLogged,
        setUserData,
    } = userStore

    const {
        isLogin,
        userData,
    } = storeToRefs(userStore)

    const {customNotify} = useCustomQuasar()

    const backendBaseurl = import.meta.env.VITE_APP_BACKEND_BASEURL

    const name = ref<string>('')
    const email = ref<string>('')
    const username = ref<string>('')
    const password = ref<string>('')
    const confirmPassword = ref<string>('')
    const message = ref<string>('')

    const emailRegex = new RegExp('(?:[a-z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])')
    const nameRegex = new RegExp(/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,} [a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,}$/u)
    const usernameRegex = new RegExp(/^[A-Za-z][A-Za-z0-9_]{5,29}$/)
    const passwordRegex = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$_ %^&*-]).{8,}$/)

    const register = async () => {
        if (password.value !== confirmPassword.value || name.value === '' || username.value === '' || username.value === '' || password.value === '') return;
        try {
            const response = await axios.post(`${backendBaseurl}auth/register/`, {
                name: name.value,
                email: email.value,
                username: username.value,
                password: password.value,
            })

            if (response.status === 201) {
                purgeState()
                customNotify('Vous êtes maintenant inscrit, vous pouvez vous connecter', 'positive')
                isLogin.value = true;
            }
        } catch (e: any) {
            if (e.response.data.email) {
                customNotify('Un utilisateur avec cette adresse email existe déjà', 'negative')
            }
            if (e.response.data.username) {
                customNotify('Un utilisateur avec ce nom d\'utilisateur existe déjà', 'negative')
            }
        }
    }

    const login = async () => {
        if (userIsLogged()) return;
        if (email.value === '' || password.value === '') return
        try {
            const response = await axios.post(`${backendBaseurl}auth/login/`, {
                email: email.value,
                password: password.value,
            })
            setJwt({...JSON.parse(window.atob(response.data.access.split('.')[1]))})
            localStorage.setItem('jwt_access', response.data.access)
            localStorage.setItem('jwt_refresh', response.data.refresh)
            purgeState()
            customNotify('Vous êtes maintenant connecté', 'positive')
            await getUserMe()
            await router.push({name: 'home'})
        } catch (e: any) {
            console.log(e)
            if (e.response.status === 401) {
                customNotify('Identifiants incorrects', 'negative')
            } else {
                customNotify('Une erreur est survenue lors de la connexion', 'negative')
            }
        }
    }

    const logout = async () => {
        localStorage.removeItem('jwt_access')
        localStorage.removeItem('jwt_refresh')
        setJwt(undefined)
        setUserData(undefined)
        if (route.meta.requiresAdmin) await router.push({name: 'home'})
        customNotify('Vous êtes maintenant déconnecté', 'info')
    }

    const getUserMe = async () => {
        if (!userIsLogged()) return;
        try {
            const response = await axios.get(`${backendBaseurl}auth/me/`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('jwt_access')}`
                }
            })
            if (response.status === 200) {
                setUserData(response.data)
            }
        } catch (e: any) {
            console.log(e.response.statusCode === 401)
            customNotify('Une erreur est survenue lors de la récupération de vos données', 'negative')
        }
    }

    const refresh = async () => {
        const jwt_refresh = localStorage.getItem('jwt_refresh')
        const response = await axios.post(backendBaseurl + 'auth/token/refresh/', {
            refresh: jwt_refresh
        })
        if (response.status === 200) {
            localStorage.setItem('jwt_access', response.data.access)
            return true
        } else if (response.status === 401) {
            return false
        }
    }


    const contact = () => {
        if (userIsLogged() && userData.value?.name) name.value = userData.value.name
        if (userIsLogged() && userData.value?.email) email.value = userData.value.email
        if (name.value === '' || email.value === '' || message.value === '') return
        axios.post(`${backendBaseurl}auth/contact/`, {
            name: name.value,
            email: email.value,
            message: message.value,
        })
        purgeState()
        message.value = ''
    }

    const purgeState = () => {
        name.value = ''
        email.value = ''
        username.value = ''
        password.value = ''
        confirmPassword.value = ''
    }

    return {
        confirmPassword,
        contact,
        email,
        emailRegex,
        getUserMe,
        login,
        logout,
        message,
        name,
        nameRegex,
        password,
        passwordRegex,
        purgeState,
        refresh,
        register,
        username,
        usernameRegex,
    }
}