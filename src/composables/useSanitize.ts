import axios from "axios";
import {useLogin} from "@/composables/useLogin.ts";
import {useUserStore} from "@/stores/useUserStore.ts";
import {useRouter} from "vue-router";

export const useSanitize = () => {

    const baseUrl = import.meta.env.VITE_APP_BACKEND_BASEURL

    const {jwtIsValid, setUserData, setJwt} = useUserStore()
    const {refresh} = useLogin()

    const sanitizeFunction = async (callback: Function) => {
        if (!jwtIsValid()) {
            if (localStorage.getItem('jwt_access')) localStorage.removeItem('jwt_access')
            if (localStorage.getItem('jwt_refresh')) localStorage.removeItem('jwt_refresh')
            setJwt(undefined)
            setUserData(undefined)
            return
        }

        const response = await axios.post(baseUrl + 'auth/token/verify/', {
            "token": localStorage.getItem('jwt_refresh')
        })
        if (response.status === 200) {
            return callback()
        } else {
            if (await refresh()) {
                return callback()
            } else {
                await useRouter().push({name: 'login'})
            }
        }
    }

    return {sanitizeFunction}
}