import axios, {AxiosResponse} from "axios";
import {ArticlesApi} from "#/Articles";
import {useArticlesStore} from "@/stores/useArticlesStore";
import {useCustomQuasar} from "@/composables/useCustomQuasar.ts";

export const useArticles = () => {

    const baseUrl = import.meta.env.VITE_APP_BACKEND_BASEURL
    const {customNotify} = useCustomQuasar()
    const dateOptions = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    }

    const articlesStore = useArticlesStore()
    const {
        setArticles,
        setArticleDetailed,
        setHighlightedArticle
    } = articlesStore

    const loadArticles = async () => {
        try {
            const highlighted: AxiosResponse<ArticlesApi> = await axios.get<ArticlesApi>(baseUrl + 'articles/highlight/')
            const articles: AxiosResponse<ArticlesApi[]> = await axios.get<ArticlesApi[]>(baseUrl + 'articles/')

            if (highlighted.status === 204) {
                if (articles.data !== undefined) {
                    setHighlightedArticle(articles.data.shift() as ArticlesApi)
                    setArticles(articles.data)
                }
            } else {
                if (!!articles.data) {
                    setHighlightedArticle(highlighted.data)
                    setArticles(articles.data.filter(obj => obj.id !== highlighted.data.id))
                }
            }
        } catch (e) {
            customNotify('Erreur de chargement des articles', 'negative')
        }
    }

    const loadArticleBySlug = async (slug: string) => {
        const response = await axios.get<ArticlesApi>(baseUrl + 'articles/article/' + slug + '/')
        if (response.data) {
            setArticleDetailed(response.data)
        } else {
            customNotify('Erreur de chargement des articles', 'negative')
        }
    }

    return {
        dateOptions,
        loadArticleBySlug,
        loadArticles
    }
}