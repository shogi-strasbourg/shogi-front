import {useQuasar} from "quasar";

export const useCustomQuasar = () => {
    const {notify} = useQuasar()

    const customNotify = (message: string, type: string) => notify({
        message,
        type,
    })

    return {customNotify}
}