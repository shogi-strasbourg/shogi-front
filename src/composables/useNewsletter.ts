import axios from "axios";
import {ref} from "vue";
import {useCustomQuasar} from "@/composables/useCustomQuasar.ts";

export const useNewsletter = () => {

    const {customNotify} = useCustomQuasar()

    const email = ref<string>('')

    const backendBaseurl = import.meta.env.VITE_APP_BACKEND_BASEURL

    const verifyNewsletter = async (token: string) => {
        try {
            return await axios.post(`${backendBaseurl}newsletter/verify/`, {
                verification_code: token,
            })
        } catch (e: any) {
            return e.response
        }

    }

    const subscribeToNewsletter = async () => {
        if (email.value === '') return

        try {
            const response = await axios.post(`${backendBaseurl}newsletter/`, {
                email: email.value
            })
            console.log(response)
            if (response.status === 201) customNotify('Un email vous a été envoyé pour valider votre inscription à la newsletter.', 'positive')
        } catch (e: any) {
            if (e.response.status === 409) customNotify('Vous êtes déjà inscrit à la newsletter', 'info')
            else {
                customNotify('Une erreur est survenue lors de l\'inscription à la newsletter', 'negative')
            }
        }
        email.value = ''
    }

    const unregisterToNewsletter = async () => {
        if (email.value === '') return

        try {
            const response = await axios.delete(`${backendBaseurl}newsletter/`, {data: {email: email.value}})
            console.log(response)
            if (response.status === 200) customNotify('Un email vous a été envoyé pour valider votre désinscription à la newsletter.', 'positive')
        } catch (e: any) {
            if (e.response.status === 404) customNotify('Vous n`êtes pas inscrit à la newsletter.', 'negative')
            else {
                customNotify('Une erreur est survenue lors de l\'inscription à la newsletter', 'negative')
            }
        }
        email.value = ''
    }

    return {
        email,
        unregisterToNewsletter,
        verifyNewsletter,
        subscribeToNewsletter,
    }
}