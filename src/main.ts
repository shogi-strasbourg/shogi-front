import {createApp} from 'vue'

import {Quasar, Notify} from 'quasar'
import quasarLang from 'quasar/lang/fr'
import '@quasar/extras/material-icons/material-icons.css'
import 'quasar/src/css/index.sass'

import '@/style.css'
import App from './App.vue'
import router from "@/router/router.ts";
import {createPinia} from "pinia";
// @ts-ignore
import VueMatomo from "vue-matomo";


const createLocalApp = () => {
    const app = createApp(App)
    if (import.meta.env.VITE_APP_ENV === 'prod') {
        app.use(VueMatomo, {
            host: 'https://analytics.shogi-strasbourg.fr',
            siteId: 1,
        })
    }
    app.use(Quasar, {
        plugins: {Notify},
        lang: quasarLang
    })
        .use(router)
        .use(createPinia())
        .mount('#app')
}

if (import.meta.env.VITE_APP_ENV === 'test') {
    if (import.meta.env.VITE_APP_PWD === prompt('Pour accéder à l\'espace de test merci de rentrer le mot de passe.')) {
        createLocalApp()
    }
} else {
    createLocalApp()
}
